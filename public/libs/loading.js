(function (lib, img, cjs, ss, an) {

    var p; // shortcut to reference prototypes
    lib.webFontTxtInst = {};
    var loadedTypekitCount = 0;
    var loadedGoogleCount = 0;
    var gFontsUpdateCacheList = [];
    var tFontsUpdateCacheList = [];
    lib.ssMetadata = [];


    lib.updateListCache = function (cacheList) {
        for (var i = 0; i < cacheList.length; i++) {
            if (cacheList[i].cacheCanvas)
                cacheList[i].updateCache();
        }
    };

    lib.addElementsToCache = function (textInst, cacheList) {
        var cur = textInst;
        while (cur != exportRoot) {
            if (cacheList.indexOf(cur) != -1)
                break;
            cur = cur.parent;
        }
        if (cur != exportRoot) {
            var cur2 = textInst;
            var index = cacheList.indexOf(cur);
            while (cur2 != cur) {
                cacheList.splice(index, 0, cur2);
                cur2 = cur2.parent;
                index++;
            }
        }
        else {
            cur = textInst;
            while (cur != exportRoot) {
                cacheList.push(cur);
                cur = cur.parent;
            }
        }
    };

    lib.gfontAvailable = function (family, totalGoogleCount) {
        lib.properties.webfonts[family] = true;
        var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];
        for (var f = 0; f < txtInst.length; ++f)
            lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);

        loadedGoogleCount++;
        if (loadedGoogleCount == totalGoogleCount) {
            lib.updateListCache(gFontsUpdateCacheList);
        }
    };

    lib.tfontAvailable = function (family, totalTypekitCount) {
        lib.properties.webfonts[family] = true;
        var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];
        for (var f = 0; f < txtInst.length; ++f)
            lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);

        loadedTypekitCount++;
        if (loadedTypekitCount == totalTypekitCount) {
            lib.updateListCache(tFontsUpdateCacheList);
        }
    };
// symbols:
// helper functions:

    function mc_symbol_clone() {
        var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
        clone.gotoAndStop(this.currentFrame);
        clone.paused = this.paused;
        clone.framerate = this.framerate;
        return clone;
    }

    function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
        var prototype = cjs.extend(symbol, cjs.MovieClip);
        prototype.clone = mc_symbol_clone;
        prototype.nominalBounds = nominalBounds;
        prototype.frameBounds = frameBounds;
        return prototype;
    }


    (lib.outer = function (mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().ls(["#EB7650", "#EA4F4F"], [0, 1], -504, 0, 504, 0).ss(75).p("EBOIAtHMAAAhaNMhOIgtHMhOHAtHMAAABaNMBOHAtHg");
        this.shape.setTransform(0, 0, 0.064, 0.064);

        this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    }).prototype = getMCSymbolPrototype(lib.outer, new cjs.Rectangle(-36.1, -41.7, 72.3, 83.5), null);


    (lib.inner = function (mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Слой 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().ls(["#EB7650", "#EA4F4F"], [0, 1], -4, 0, 4, 0).ss(75).p("AAA6CMAAAA0F");
        this.shape.setTransform(-18.6, 0, 0.064, 0.064);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f().ls(["#EB7650", "#EA4F4F"], [0, 1], -148.3, 0, 148.3, 0).ss(75).p("A2iNBMAtFgaB");
        this.shape_1.setTransform(9.3, 16.1, 0.064, 0.064);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().ls(["#EB7650", "#EA4F4F"], [0, 1], -148.3, 0, 148.3, 0).ss(75).p("AWjNBMgtFgaB");
        this.shape_2.setTransform(9.3, -16, 0.064, 0.064);

        this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    }).prototype = getMCSymbolPrototype(lib.inner, new cjs.Rectangle(-22.5, -25.4, 45.1, 50.8), null);


// stage content:
    (lib.loading = function (mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // outer
        this.instance = new lib.inner();
        this.instance.parent = this;
        this.instance.setTransform(50.9, 50);

        this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation: -90}, 19).wait(20).to({rotation: 0}, 19).wait(22));

        // inner
        this.instance_1 = new lib.outer();
        this.instance_1.parent = this;
        this.instance_1.setTransform(50.9, 50.1);

        this.timeline.addTween(cjs.Tween.get(this.instance_1).to({rotation: 90}, 19).wait(20).to({rotation: 0}, 19).wait(22));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(64.8, 58.4, 72.3, 83.4);
// library properties:
    lib.properties = {
        width: 100,
        height: 100,
        fps: 60,
        color: "#FFFFFF",
        opacity: 1.00,
        webfonts: {},
        manifest: [],
        preloads: []
    };


})(lib = lib || {}, images = images || {}, createjs = createjs || {}, ss = ss || {}, AdobeAn = AdobeAn || {});
var lib, images, createjs, ss, AdobeAn;

var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
function init() {
    canvas = document.getElementById("canvas");
    anim_container = document.getElementById("animation_container");
    dom_overlay_container = document.getElementById("dom_overlay_container");
    handleComplete();
}
function handleComplete() {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    exportRoot = new lib.loading();
    stage = new createjs.Stage(canvas);
    stage.addChild(exportRoot);
    //Registers the "tick" event listener.
    fnStartAnimation = function () {
        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener("tick", stage);
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
        var lastW, lastH, lastS = 1;
        window.addEventListener('resize', resizeCanvas);
        resizeCanvas();
        function resizeCanvas() {
            var w = lib.properties.width, h = lib.properties.height;
            var iw = window.innerWidth, ih = window.innerHeight;
            var pRatio = window.devicePixelRatio || 1, xRatio = iw / w, yRatio = ih / h, sRatio = 1;
            if (isResp) {
                if ((respDim == 'width' && lastW == iw) || (respDim == 'height' && lastH == ih)) {
                    sRatio = lastS;
                }
                else if (!isScale) {
                    if (iw < w || ih < h)
                        sRatio = Math.min(xRatio, yRatio);
                }
                else if (scaleType == 1) {
                    sRatio = Math.min(xRatio, yRatio);
                }
                else if (scaleType == 2) {
                    sRatio = Math.max(xRatio, yRatio);
                }
            }
            canvas.width = w * pRatio * sRatio;
            canvas.height = h * pRatio * sRatio;
            canvas.style.width = dom_overlay_container.style.width = anim_container.style.width = w * sRatio + 'px';
            canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h * sRatio + 'px';
            stage.scaleX = pRatio * sRatio;
            stage.scaleY = pRatio * sRatio;
            lastW = iw;
            lastH = ih;
            lastS = sRatio;
        }
    }

    makeResponsive(true, 'both', false, 1);
    fnStartAnimation();
}