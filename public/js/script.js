// ПРЕДЗАГРУЖАЕМЫЕ ИЗОБРАЖЕНИЯ
var preloads = [
    [
        'images/sprite.png',
        'images/shane.png'
    ],
    [
        'sounds/1.wav',
        'sounds/2.wav',
        'sounds/3.wav',
        'sounds/4.wav',
        'sounds/5.wav'
    ]
];


// ВСЕ НАДПИСИ
var tags = [
    '— will not make you happier',
    '— all you need',
    '— won\'t help you',
    '— the only thing that makes you happy'
];


var loaded = [[], []];  // МАССИВ ИАЗРУЖЕННЫХ ЭЛЕМНТОВ


// ГРОМКОСТЬ
var volumeLevel = 0.1;


// ПРОВЕРКА ЗАГРУЗКИ ИЗОБРАЖЕНИЙ
function checkImages(imagesCounter) {
    if (imagesCounter === preloads[0].length) {
        pageLoaded();
    }
}


// ПРЕДЗАГРУЗКА ИЗОБРАЖЕНИЙ
function preloadImages() {
    var imagesCounter = 0;

    for (var i = 0; i < preloads[0].length; i++) {
        loaded[0][i] = new Image();
        loaded[0][i].onload = (function () {
            imagesCounter++;
            checkImages(imagesCounter);
        });
        loaded[0][i].src = preloads[0][i];
    }
}


// ПРЕДЗАГРУЗКА ЗВУКОВ
function preloadSounds() {
    var soundsCounter = 0;
    for (var i = 0; i < preloads[1].length; i++) {
        loaded[1][i] = new Audio();
        loaded[1][i].src = preloads[1][i];
    }
}


// GLITCH-ЭФФЕКТ
function glitch() {

    // ОПРЕДЕЛЕНИЕ ПЕРЕМЕННЫХ
    var image = Math.round(Math.random() * 2) + 1;           // ИЗОБРАЖЕНИЕ
    var sound = Math.round(Math.random() * 4) + 1;           // ЗВУК
    var tag = Math.round(Math.random() * 2);                 // НАДПИСЬ
    var interval = Math.round(Math.random() * 2000) + 1000;  // ИНТЕРВАЛ
    var duration = Math.round(Math.random() * 200) + 50;     // ПРОДОЛЖИТЕЛЬНОСТЬ
    var audio = new Audio('sounds/' + sound + '.wav');       // СОЗДАНИЕ ЗВУКА
    var width = $('.glitch').parent().width();

    // УСТАНОВКА ЗНАЧЕНИЙ
    $('.tag > h2').text(tags[tag]);                              // НАДПИСЬ
    $('.glitch').css('object-position', image * -width + 'px');  // ПОЛОЖЕНИЕ ФОНА
    audio.volume = volumeLevel;                                  // ГРОМКОСТЬ
    audio.play();                                                // ПРОИГРЫВАНИЕ

    // ОСТАНОВКА
    setTimeout(function () {
        $('.tag > h2').text(tags[3]);
        $('.glitch').css('object-position', 0);
        audio.pause();
    }, duration);

    // ВОЗВРАТ
    setTimeout(function () {
        glitch();
    }, interval);

    /* ОТЛАДКА
     console.log(image * 100 / 3 + ' ' + sound + ' ' + tags[tag] + ' ' + duration + ' ' + interval);
     */
}


// ЗАКРЫТИЕ БИОГРАФИИ
function hideBiography() {
    $('.up, .background').fadeOut(250);
    $('.glitch').removeClass('glitch');
    $('.up').removeClass('up');
}


// ФУНКЦИЯ ПРИ ЗАГРУЗКЕ СТРАНИЦЫ
function pageLoaded() {
    $('.loading-screen > h1').remove();
    $('.loading-screen').animate({
        opacity: 0
    }, 1000, function () {
        $(this).remove();
    });
    setTimeout(function () {
        glitch();
    }, 2500);
}


// ПОЛОЖЕНИЕ БИОГРАФИЯ
function biographyPosition(element) {
    var height = element.outerHeight();
    element.css({
        marginTop: -height / 2 + 'px',
        marginBottom: -height / 2 + 'px'
    });
}


// ПРИ ИЗМЕНЕНИИ ЭКРАНА
$(window).on('resize', function () {
    biographyPosition($('.up'));
});


// ВЫБОР БИОГРАФИИ
$('.members ul li').on('click', function () {
    $('.members ul li').removeClass('selected');
    $(this).addClass('selected');
    var number = $(this).index();
    var element = $('.biography-wrapper:eq(' + number + ')');
    var photo = element.find('.photo');

    if ($('.up').length) {
        hideBiography();
    }

    $('.logo').removeClass('glitch').css('object-position', 0);
    $('.background').fadeIn(250);
    element.fadeIn(250);
    element.addClass('up');
    photo.addClass('glitch');
    biographyPosition(element);
});


// НАЖАТИЕ НА НАЗВАНИЕ
$('.tag h1, button, .background').on('click', function () {
    hideBiography();
    $('.members ul li').removeClass('selected');
    $('.logo').css('opacity', 1).addClass('glitch');
});


// ВКЛЮЧЕНИЕ/ВЫКЛЮЧЕНИЕ ЗВУКА
$('.sound').on('click', function () {
    if (volumeLevel === 0) {
        volumeLevel = 0.1;
        $(this).find('span').text('on');
    }
    else {
        volumeLevel = 0;
        $(this).find('span').text('off');
    }
});


// ПОСЛЕ ЗАГРУЗКИ СТРАНИЦЫ
$(document).ready(function () {
    init();
    preloadImages();
    //preloadSounds();
});
