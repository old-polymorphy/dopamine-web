<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property Carbon date
 */
class Announcement extends Model

{
    protected $dates = ['date'];
    protected $appends = ['monthName', 'dayNumber', 'weekDay'];

    /**
     * Return the announcement's moth's name.
     *
     * @return string
     */
    public function getMonthNameAttribute(): string
    {
        return date('M', strtotime($this->getAttribute('date')));
    }

    /**
     * Return the announcement's day's number.
     *
     * @return string
     */
    public function getDayNumberAttribute(): string
    {
        return $this->date->day;
    }

    /**
     * Return the announcement's day's number.
     *
     * @return string
     */
    public function getWeekDayAttribute(): string
    {
        return date('D', strtotime($this->date));
    }
}



