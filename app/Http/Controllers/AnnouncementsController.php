<?php

namespace App\Http\Controllers;


use App\Announcement;
use Carbon\Carbon;

class AnnouncementsController
{
    public function index()
    {
        $events = Announcement::all()->where('date', '>=', Carbon::now());
        return view('tour', compact('events'));
    }
}