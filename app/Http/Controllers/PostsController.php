<?php

namespace App\Http\Controllers;

use App\Post;

class PostsController extends Controller
{
    /**
     * Index method.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('news');
    }

    /**
     * Get posts.
     *
     * @param $number
     * @return mixed
     */
    public function get($number)
    {
        return Post::orderBy('id', 'desc')->limit($number)->get();
    }

    /**
     * Get 4 more posts.
     *
     * @param $id
     * @return mixed
     */
    public function more($id)
    {
        return Post::orderBy('id', 'desc')->where('id', '<', $id)->limit(4)->get();
    }

    /**
     * Count posts.
     *
     * @return int
     */
    public function count()
    {
        return Post::all()->count();
    }

    /**
     * @param Post $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Post $post)
    {
        return view('post', compact('post'));
    }
}
