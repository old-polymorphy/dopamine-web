<?php

use Faker\Generator as Faker;

$factory->define(\App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'body' => $faker->realText(500),
        'picture' => $faker->imageUrl($width = 640, $height = 480),
    ];
});
