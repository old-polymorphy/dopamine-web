<?php

use App\Announcement;
use Faker\Generator as Faker;

$factory->define(Announcement::class, function (Faker $faker) {
    return [
        'date' => $faker->dateTimeBetween('+730 days', '+1460 days'),
        'city' => $faker->city,
        'place' => $faker->company,
    ];
});
