<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('news/get/{number}', 'PostsController@get');

Route::get('news/more/{id}', 'PostsController@more');

Route::get('news/count', 'PostsController@count');

Route::get('announcements', function () {
    return App\Announcement::orderBy('id', 'asc')
        ->where('date', '>=', \Carbon\Carbon::now())
        ->limit(5)
        ->get();
});