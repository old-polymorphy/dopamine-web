<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
});

Route::get('/news', 'PostsController@index')->name('news');

Route::get('/post/{post}', 'PostsController@show')->name('news');

Route::get('/tour', 'AnnouncementsController@index')->name('tour');

Route::get('/music', function () {
    return view('music');
})->name('music');

Route::get('/bio', function () {
    return view('bio');
})->name('bio');

Route::get('/gallery', function () {
    return view('gallery');
})->name('gallery');