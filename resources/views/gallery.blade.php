@extends('layouts.layout')

{{-- Title --}}
@section('title')
    Gallery
@endsection

@section('content')

    <div class="content">

        <h2>Gallery</h2>
        <div>
            <div class="gallery-element">
                <img src="/images/1.jpg">
            </div>
            <div class="gallery-element">
                <img src="/images/2.jpg">
            </div>
            <div class="gallery-element">
                <img src="/images/3.jpg">
            </div>
            <div class="clear"></div>
        </div>

    </div>

@endsection