@extends('layouts.layout')

{{-- Title --}}
@section('title')
    Upcoming events
@endsection

@section('content')
    <div class="content">
        <h2>Upcoming events</h2>

        {{-- Events wrapper --}}
        <div class="events">
            @foreach($events as $event)

                {{-- Announcements --}}
                <div class="event">

                    {{-- Date --}}
                    <div class="date">
                        <span>{{ $event->monthName }} {{ $event->dayNumber }},
                            {{ $event->weekDay }}</span>
                    </div>

                    {{-- Location --}}
                    <div class="location">
                        <span>{{ $event->city }}</span> / <span class="club">{{ $event->place }}</span>
                    </div>

                    {{-- Tickets --}}
                    <a class="tickets" href="#">Tickets</a>

                    <div style="clear: both"></div>
                </div>
            @endforeach
        </div>

    </div>
@endsection