@extends('layouts.layout')

{{-- Title --}}
@section('title')
    Dopamine
@endsection

{{-- Home page content --}}
@section('content')

    {{-- Glitching logo --}}
    <img alt="logo" src="images/sprite.png" class="logo glitch">

    {{-- Content --}}
    <div class="content main-page">

        {{-- Posts --}}
        <div class="posts">
            <h2>
                <a href="/news">News</a>
            </h2>

            {{-- Component --}}
            <news></news>

        </div>

        {{-- Announcements --}}
        <div class="announcements">
            <h2>
                <a href="/tour">Announcements</a>
            </h2>

            {{-- Component --}}
            <announcements></announcements>

            <a href="/tour">All upcoming events ›</a>
        </div>

        <div style="clear: both"></div>
    </div>

@endsection