@extends('layouts.layout')

{{-- Title --}}
@section('title')
    {{ $post->title }}
@endsection

@section('content')

    <div class="content">
        <h2>{{ $post->title }}</h2>

        {{ $post->body }}

    </div>

@endsection
