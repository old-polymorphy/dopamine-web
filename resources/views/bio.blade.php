@extends('layouts.layout')

{{-- Title --}}
@section('title')
    Biography
@endsection

@section('content')

    <div class="content">

        <h2>Biography</h2>

        <div class="bio">

            {{-- First person --}}
            <div class="bio__element">
                <div class="bio__container">

                    {{-- Photo --}}
                    <div class="bio__photo">
                        <img alt="person" class="glitch" src="images/shane.png">
                    </div>

                    <div class="bio__information">
                        <h2>Lorem Ipsum</h2>

                        <p class="bio__summary">Bio: Aliquam pretium interdum vestibulum. Suspendisse nec fringilla
                            purus.
                            Etiam blandit purus urna, eget
                            elementum massa lobortis at. Donec vehicula tortor eros, fermentum pulvinar diam congue
                            semper.
                            Sed vel erat
                            arcu.</p>

                        <p class="bio__sub">“Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,
                            consectetur,
                            adipisci velit...”</p>
                    </div>

                </div>

            </div>

            {{-- Second person --}}
            {{--<div class="bio__element">--}}
                {{--<div class="bio__container">--}}

                    {{-- Photo --}}
                    {{--<div class="bio__photo">--}}
                        {{--<img alt="person" class="glitch" src="images/shane.png">--}}
                    {{--</div>--}}

                    {{--<div class="bio__information">--}}
                        {{--<h2>Lorem Ipsum</h2>--}}

                        {{--<p class="bio__summary">Bio: Aliquam pretium interdum vestibulum. Suspendisse nec fringilla--}}
                            {{--purus.--}}
                            {{--Etiam blandit purus urna, eget--}}
                            {{--elementum massa lobortis at. Donec vehicula tortor eros, fermentum pulvinar diam congue--}}
                            {{--semper.--}}
                            {{--Sed vel erat--}}
                            {{--arcu.</p>--}}

                        {{--<p class="bio__sub">“Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,--}}
                            {{--consectetur,--}}
                            {{--adipisci velit...”</p>--}}
                    {{--</div>--}}

                {{--</div>--}}

            {{--</div>--}}

            {{-- Third person --}}
            {{--<div class="bio__element">--}}
                {{--<div class="bio__container">--}}

                    {{-- Photo --}}
                    {{--<div class="bio__photo">--}}
                        {{--<img alt="person" class="glitch" src="images/shane.png">--}}
                    {{--</div>--}}

                    {{--<div class="bio__information">--}}
                        {{--<h2>Lorem Ipsum</h2>--}}

                        {{--<p class="bio__summary">Bio: Aliquam pretium interdum vestibulum. Suspendisse nec fringilla--}}
                            {{--purus.--}}
                            {{--Etiam blandit purus urna, eget--}}
                            {{--elementum massa lobortis at. Donec vehicula tortor eros, fermentum pulvinar diam congue--}}
                            {{--semper.--}}
                            {{--Sed vel erat--}}
                            {{--arcu.</p>--}}

                        {{--<p class="bio__sub">“Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,--}}
                            {{--consectetur,--}}
                            {{--adipisci velit...”</p>--}}
                    {{--</div>--}}

                {{--</div>--}}

            {{--</div>--}}

            {{-- Fourth person --}}
            {{--<div class="bio__element">--}}
                {{--<div class="bio__container">--}}

                    {{-- Photo --}}
                    {{--<div class="bio__photo">--}}
                        {{--<img alt="person" class="glitch" src="images/shane.png">--}}
                    {{--</div>--}}

                    {{--<div class="bio__information">--}}
                        {{--<h2>Lorem Ipsum</h2>--}}

                        {{--<p class="bio__summary">Bio: Aliquam pretium interdum vestibulum. Suspendisse nec fringilla--}}
                            {{--purus.--}}
                            {{--Etiam blandit purus urna, eget--}}
                            {{--elementum massa lobortis at. Donec vehicula tortor eros, fermentum pulvinar diam congue--}}
                            {{--semper.--}}
                            {{--Sed vel erat--}}
                            {{--arcu.</p>--}}

                        {{--<p class="bio__sub">“Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,--}}
                            {{--consectetur,--}}
                            {{--adipisci velit...”</p>--}}
                    {{--</div>--}}

                {{--</div>--}}

            {{--</div>--}}

        </div>

    </div>

@endsection