<!DOCTYPE html>
<html lang="en">
<head>

    {{-- Meta --}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- Yandex Webmaster --}}
    <meta name="yandex-verification" content="1318b507275f39fa"/>

    {{-- Title --}}
    <title>@yield('title')</title>

    {{-- Links --}}
    <link rel="stylesheet" href="{{'/css/styles.css'}}">
    <link rel="icon" type="image/x-icon" href="{{'favicon.ico'}}">

</head>

<body>

<?php $routeName = request()->route()->getName(); ?>

{{-- Main --}}
<div class="wrapper">
    <main id="app">

        <header>

            {{-- Tagline --}}
            <div class="tag">
                <h1>
                    <a href="/">dopamine</a>
                </h1>
                <h2>— the only thing that makes you happy</h2>
            </div>

            {{-- Navigation --}}
            <nav class="navigation">
                <a href="{{'/news'}}" class="{{ $routeName === 'news' ? 'active' : '' }}">news</a>
                <a href="{{'/tour'}}" class="{{ $routeName === 'tour' ? 'active' : '' }}">tour</a>
                <a href="{{'/music'}}" class="{{ $routeName === 'music' ? 'active' : '' }}">music</a>
                <a href="{{'/bio'}}" class="{{ $routeName === 'bio' ? 'active' : '' }}">bio</a>
                <a href="{{'/store'}}" class="{{ $routeName === 'store' ? 'active' : '' }}">store</a>
                <a href="{{'/gallery'}}" class="{{ $routeName === 'gallery' ? 'active' : '' }}">gallery</a>
                <a href="{{'/links'}}" class="{{ $routeName === 'links' ? 'active' : '' }}">links</a>
            </nav>


            {{-- Hamburger --}}
            <button class="hamburger hamburger--elastic" type="button">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>

            <div style="clear: both"></div>
        </header>

        {{-- Content --}}
        @yield('content')

    </main>

    {{-- Footer --}}
    <footer class="footer">

        <div class="footer__wrapper">

            {{-- Copyright --}}
            <div class="copyright">
                <a href="https://polymorphy.ru/" target="_blank">Polymorphy</a> © 2017
            </div>

            <!-- Sound -->
            <div class="sound">
                sound: <span>off</span>
            </div>

            <div style="clear: both"></div>
        </div>

        <!-- Line -->
        <div class="line"></div>

    </footer>
</div>

<div class="background"></div>

{{-- Scripts --}}
<script src="{{'/js/app.js'}}"></script>

{{-- Yandex --}}
@if (env('APP_ENV') === 'production')
    @include('statistics.yandex')
@endif

</body>
</html>
