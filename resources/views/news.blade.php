@extends('layouts.layout')

{{-- Title --}}
@section('title')
    News
@endsection

@section('content')

    <div class="content news-page">

        {{-- Posts --}}
        <div class="posts">
            <h2>News</h2>

            {{-- News component --}}
            <news></news>

        </div>

    </div>

@endsection