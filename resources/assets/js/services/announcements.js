// Libs
import axios from 'axios';

export default {
  /**
   * Fetches announcements.
   *
   * @returns {AxiosPromise<void>}
   */
  getAnnouncements() {
    return axios.get('api/announcements');
  }
};