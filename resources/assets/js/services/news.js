// Libs
import axios from 'axios';

export default {
  /**
   * Fetches the specified number of news.
   *
   * @param {number} number — Number of posts.
   * @returns {AxiosPromise<void>}
   */
  getNews(number) {
    return axios.get(`/api/news/get/${number}`);
  },

  /**
   * Counts total amount of news.
   *
   * @returns {AxiosPromise<void>}
   */
  countNews() {
    return axios.get('api/news/count');
  },

  /**
   * Fetches more news.
   *
   * @param {number} last — ID of the last post.
   * @returns {Promise<any>}
   */
  moreNews(last) {
    return axios.get(`api/news/more/${last}`);
  },
};