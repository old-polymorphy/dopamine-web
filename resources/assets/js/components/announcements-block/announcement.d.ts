declare class Announcement {
  city: string;
  created_at: string;
  date: string;
  id: number;
  place: string;
  updated_at: string;
}