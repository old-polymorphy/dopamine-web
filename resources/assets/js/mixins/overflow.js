export default {
  mounted() {
    this.isOverflow();
  },

  methods: {
    isOverflow() {
      let element = this.$el;
      let paragraph = element.getElementsByTagName('p')[1];
      let a = paragraph.getElementsByTagName('a')[0];
      if (paragraph.scrollHeight > 111) {
        a.style.display = 'block';
      }
    }
  }
};