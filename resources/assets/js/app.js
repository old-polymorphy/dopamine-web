import './bootstrap';

// Libs
import Vue from 'vue';

// Components
import News from './components/posts-block/posts-block.vue';
import Post from './components/post-component/post-component.vue';
import Announcements from './components/announcements-block/announcements-block.vue';

// Vue instance
new Vue({
  el: '#app',

  components: {
    News,
    Post,
    Announcements
  }
});

// Writings
const tags = [
  '— will not make you happier',
  '— all you need',
  '— won\'t help you',
  '— the only thing that makes you happy'
];

// Volume
let volumeLevel = 0;

/**
 * Glitches.
 *
 * @returns {void}
 */
function glitch() {
  // Vars
  const image = Math.round(Math.random() * 2) + 1;
  const sound = Math.round(Math.random() * 4) + 1;
  const tag = Math.round(Math.random() * 2);
  const interval = Math.round(Math.random() * 2000) + 1000;
  const duration = Math.round(Math.random() * 200) + 50;
  const audio = new Audio('sounds/' + sound + '.wav');
  const width = document.querySelector('.glitch').parentElement.clientWidth;

  // Setting new values
  document.querySelector('.tag > h2').innerHTML = tags[tag]; // Writing
  document.querySelectorAll('.glitch').forEach((element) => {
    element.style.objectPosition = image * -width + 'px';
    audio.volume = volumeLevel;
  });

  // Playing a sound
  if (volumeLevel) {
    audio.play()
      .catch((e) => {
        console.error(e);
      });
  }

  // Stopping
  setTimeout(() => {
    document.querySelector('.tag > h2').innerHTML = tags[3];
    document.querySelectorAll('.glitch')
      .forEach(element => {
        element.style.objectPosition = 0;
      });
    audio.pause();
  }, duration);

  // Playing again
  setTimeout(() => {
    glitch();
  }, interval);
}

/**
 * Sets up glitch effect.
 *
 * @returns {void}
 */
function pageLoaded() {
  if (document.querySelector('.glitch')) {
    setTimeout(glitch, 2500);
  }

  handleHamburger();
}

function handleHamburger() {
  document.querySelector('.hamburger')
    .addEventListener('click', function () {
      this.classList.toggle('is-active');

      document.querySelector('.navigation')
        .classList.toggle('navigation__active');

      document.querySelector('.background')
        .classList.toggle('background__active');
    });
}

// Sound control
document.querySelector('.sound')
  .addEventListener('click', function () {
    if (volumeLevel === 0) {
      volumeLevel = 0.1;
      this.querySelector('span').innerText = 'on';
    } else {
      volumeLevel = 0;
      this.querySelector('span').innerText = 'off';
    }
  });

document.addEventListener('DOMContentLoaded', pageLoaded);